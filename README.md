# Social MMO #

This is the project page for the Social MMO Project.

## Game details ##

This is an abstract of the game details, for the full GDD see: Game design document
### Main features ###

* A City planning simulation game.
* 3D graphics.
* Round based gameplay where each game round is two weeks.
* Multiplayer only.
* Realtime city planning.
* City is developed using resources also updated in realtime.

For more information please see the project [wiki](https://bitbucket.org/bytesizedgames/socialmmoserver/wiki/Home)