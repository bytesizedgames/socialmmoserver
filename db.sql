PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
--Create the users table
CREATE TABLE users(id INTEGER PRIMARY KEY, username VARCHAR(25), password blob, email VARCHAR(50), salt blob);
--Insert two default users.
INSERT INTO "users" VALUES(2,'Shiratori',X'35A0C801496456D1E302712A72B0B17F0118CFB6AC3DDEF7E86656929D0248D5','es.linus@gmail.com',X'6B78D83C61073493F7497CAC55EA19F8BB16283BA876FFD534C07F155333541D');
INSERT INTO "users" VALUES(3,'Guding',X'A8594E4D2B197CDCB577E3913B860074BA5E8FE5B8A157C394A0FDA10E49C3C5','guding@gmail.com',X'1720A9CCB1B19D61C8430B6217DF6368BF02650028E1EF0C8CDAB1942A73676F');
CREATE TABLE tokens (userId integer Primary Key, token blob, expiration timestamp);
--Create the cities table
CREATE TABLE cities (
id integer Primary Key, --id is the same as the user id of the player owning the city.
name varchar(15),
land double default 100,
freeLand double default 100,
money double default 1000,
population double default 100,
freePopulation double,
mass double default 1000,
energy double,
freeEnergy double,
landRate double,
moneyRate double,
populationRate double,
massRate double,
lastStatusUpdate timestamp
);

--Create the buildings table
CREATE TABLE buildings (
id integer Primary key,
name varchar (15),
tier integer,
requiredLand integer,
requiredMoney integer,
requiredMass integer,
operatingEnergy integer,
requiredPopulation integer,
buildTime integer
);

CREATE TABLE houses (
id integer primary key, --id is the same id as the buildings id of the specific housing.
houseCapacity integer
);

CREATE TABLE miners (
id integer primary key, --id is the same id as the buildings id of the specific building.
miningRate integer
);

CREATE TABLE landGrabbers (
id integer primary key, --id is the same id as the buildings id of the specific building.
grabRate integer
);

CREATE TABLE traders (
id integer primary key, --id is the same id as the buildings id of the specific building.
moneyRate integer,
tradeCapacity integer
);

COMMIT;
