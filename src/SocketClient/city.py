#Collection of commands.

#Create a city with the cityname.
def createCity():
    command = bytearray()
    command.append(0x1F)
    command.append(0x02)
    command.append(0x01)
    command.append('C')
    command.append(0x08)
    command.extend('BestCity'.encode('ascii'))
    command.append('C')

    return command

#Delete the city of the user.
def deleteCity():
    command = bytearray()
    command.append(0x1F)
    command.append(0x01)
    command.append(0x01)
    command.append('D')
    command.append('C')
    command.append(0x04)

    return command
