from twisted.internet import reactor
from twisted.internet.protocol import Factory, Protocol
from twisted.internet.endpoints import TCP4ClientEndpoint
import city

class Greeter(Protocol):

    #Connected to the server
    def connectionMade(self):

        #User credentials
        username = 'Shiratori'
        token = '912fa3bb4e5dc6c4'

        #Start building the command to send.
        command = bytearray()
        #Get statments and extend the command with them.
        command.extend(city.createCity())
        #Finish the command.
        command.append(0x04)

        #Start building the message.
        message = bytearray()

        #Start of header
        message.append(0x01)
        message.append(len(command))
        message.append(len(username))
        message.extend(username.encode('ascii'))
        message.extend(token.encode('ascii'))

        #Start of command
        message.append(0x02)
        message.extend(command)

        #Send the message.
        self.transport.write(str(message))

    def dataReceived(self, data):

        print data

class GreeterFactory(Factory):
    def buildProtocol(self, addr):
        return Greeter()

point = TCP4ClientEndpoint(reactor, "localhost", 6000)
d = point.connect(GreeterFactory())
reactor.run()
