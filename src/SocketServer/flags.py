from enum import Enum

#Hex representation of different protocol flags.

#Command flags.
class CommandFlag(Enum):
    StartStatement = 0x1F
    EndOfTransmission = 0x04

#Parameter flags.
class ParameterFlag(Enum):
    String = 0x53
    Int = 0x49
    Double = 0x44

class StatementFlag(Enum):
    City = 0x43
    Building = 0x42
