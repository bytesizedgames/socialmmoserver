from twisted.internet.protocol import Factory, Protocol
from twisted.internet import reactor
from twisted.internet.serialport import SerialPort
import messageReader

class GameProtocol(Protocol):
    def __init__(self,clients):
        print 'new protocol instance'
        self.clients = clients

    def connectionMade(self):
        self.clients.append(self)
        print 'A client has connected'
        print "clients are ", self.clients
    def connectionLost(self, reason):
        self.clients.remove(self)
        print 'A client disconnected'
        print "clients are ", self.clients
    def dataReceived(self, data):

        print 'Message recieved!'

        messageReader.read(data, self.transport)

class GameFactory(Factory):
    def __init__(self):
        print 'initing'
        self.clients = []
    def buildProtocol(self, addr):
        return GameProtocol(self.clients)

reactor.listenTCP(6000,GameFactory())
print 'server started on port 6000'
reactor.run()
