import database
import flags
from enum import Enum

class CityFlag(Enum):
    CreateCity = 'C'
    DeleteCity = 'D'

#Statements related to cities.

def city(userId, parameterList):
    flag = parameterList.popleft()
    if flag == CityFlag.CreateCity:
        create(userId, parameterList)
    if flag == CityFlag.DeleteCity:
        delete(userId)

#Creates a city for the user.
def create(userId, parameterList):
    conn = database.connect()
    c = conn.cursor()

    c.execute("select * from cities where id=?", (userId, ))
    row = c.fetchone()

    if not row is None:
        print 'City already exists.'
    else:
        c.execute("insert into cities (id, name) values (?, ?)", (userId, str(parameterList.pop())))
        conn.commit()
        conn.close()

        print 'City created!'

#Deletes the users city.
def delete(userId):
    conn = database.connect()
    c = conn.cursor()

    c.execute("delete from cities where id=?", (userId, ))
    conn.commit()
    conn.close()
    print 'City deleted'
