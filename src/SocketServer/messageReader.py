import datetime
import protocol
import database

#Read a incoming Message.
def read(message, transport):

    #Variable to store the command.
    data = bytearray()
    count = 0
    username = ''
    usernameLength = 0
    messageLength = 0

    print str(message)

    token = ''
    #Loop through the message, validating the header and then storing the command.
    for byte in message:
        if count == 0:
            print hex(ord(byte))
        elif count == 1:
            messageLength = ord(byte)
        elif count == 2:
            usernameLength = ord(byte) + 3
        elif count < usernameLength:
            username += chr(ord(byte))
        elif count < usernameLength + 16:
            token += chr(ord(byte))
        elif count == usernameLength + 16:
            print hex(ord(byte))
        else:
            #Append each byte to the command.
            data.append(byte)

        count += 1

    #Check if the command is of valid length so nothing went wrong.
    if not len(data) == messageLength:
        print 'Message is wrong length!'
    else:
        print 'Message is: ' + data

    #Validate the token.
    conn = database.connect()
    c = conn.cursor()

    c.execute('select userId, token from users, tokens where id=userId and upper(username)=upper(?)', (username, ))

    row = c.fetchone()

    if not row is None:
        userId = row[0]
        currentToken = row[1]

        #Compare the token with the stored token for that user.
        difference = len(token) ^ len(currentToken)
        for i in range(0, min([len(token), len(currentToken)])-1):
            difference |= (ord(token[i]) ^ ord(currentToken[i]))
        if difference == 0:
            print 'Token is correct!'
            #Update the expiration of the users token.
            c.execute('update tokens set expiration=? where userId=?', (datetime.datetime.now() + datetime.timedelta(minutes = 30), userId))
            conn.commit()
            conn.close()

            if len(data) > 0:
                #Pass the command along to the protocol reader.
                protocol.readCommand(userId, data, transport)
            else:
                print 'No command recieved!'
        else:
            print 'Token is incorrect!'
    else:
        print 'User has no Token!'
