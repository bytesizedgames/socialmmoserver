import flags
import database
import city
import building
from collections import deque

#Reads an incoming command, parsing each statement.
def readCommand(userId, command, transport):
    conn = database.connect()
    c = conn.cursor()

    statementStarted = False
    collectingParameters = False
    collectingParametersFinished = False
    parameterStarted = False
    parameterCount = 0
    currentParameterCount = 0
    parameters = 0
    paramLength = 0
    parameterList = deque([])
    currentParam = bytearray()

    #Start reading throught the command, byte for byte.
    for byte in command:

        #We are not currently reading a statement and we got the StartStatement flag.
        if byte == flags.CommandFlag.StartStatement and not statementStarted:
            statementStarted = True
            continue
            
        #Get the number of parameters in the statement.
        elif statementStarted and not collectingParameters and not collectingParametersFinished:
            parameters = byte
            if parameters > 0:
                collectingParameters = True
            else:
                collectingParametersFinished = True
            continue

        #Get the byte length of the parameter.
        elif statementStarted and collectingParameters and not parameterStarted and not collectingParametersFinished:
            paramLength = byte
            parameterStarted = True
            continue

        #Append each byte to the parameter until it is full, then add it to the list of parameters.
        #If all parameters are full, we are done reading parameters and we wait for the flag representing what statement it is.
        elif statementStarted and not collectingParametersFinished:
            currentParam.append(byte)
            currentParameterCount += 1

            if not currentParameterCount < paramLength:
                parameterList.append(currentParam)

                parameterStarted = False
                currentParameterCount = 0
                parameterCount += 1
                currentParam = bytearray()
                if not parameterCount < parameters:
                    collectingParameters = False
                    collectingParametersFinished = True
            continue

        #Check the statement flag and call the appropriate statement.
        elif statementStarted:

            if byte == flags.StatementFlag.City:

                city.city(userId, parameterList)

            elif byte == flags.StatementFlag.Building:

                building.building(userId, parameterList)

            statementStarted = False
            collectingParametersFinished = False
            currentParam = bytearray()
            parameterList = deque([])
            parameterCount = 0
            continue

        #EndOfTransmission recieved and we stop reading the command.
        if byte == flags.CommandFlag.EndOfTransmission:
            print 'End of command reached'
            break
