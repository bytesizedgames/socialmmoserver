import database
from enum import Enum

class BuildingFlag(Enum):
    Build = 'B'
    Activate = 'A'
    Deactivate = 'D'

def building(userId, parameterList):
    flag = parameterList.popleft()
    if flag == BuildingFlag.Build:
        build(userId, parameterList)
    if flag == BuildingFlag.Activate:
        activate(userId, parameterList)
    if flag == BuildingFlag.Deactivate:
        deactivate(userId, parameterList)

def build(userId, parameterList):
    conn = database.connect()
    c = conn.cursor()

    c.execut("select ")
    print 'Building succesdully built!'

def activate(userId, parameterList):
    return None

def deactivate(userId, parameterList):
    return None
