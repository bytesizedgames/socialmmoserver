import database
import datetime
import time

def verifyTokens():

    while True:
        now = datetime.datetime.now()

        conn = database.connect()
        c = conn.cursor()

        c.execute("delete from tokens where expiration < ?", (now, ))
        conn.commit()
        conn.close()

        time.sleep(60)

if __name__ == '__main__':
    verifyTokens()
